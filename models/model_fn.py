from tensorflow.python.estimator.export import export_output
from util import init_embeddings, count_lines
import tensorflow as tf
import tensorflow_hub as hub


def metrics(label_ids, predictions):
    return {
        'accuracy': tf.metrics.accuracy(
            labels=label_ids,
            predictions=predictions
        ),
        'precision_0': tf.metrics.precision(
            labels=tf.equal(label_ids, 0),
            predictions=tf.equal(predictions, 0)
        ),
        'recall_0': tf.metrics.recall(
            labels=tf.equal(label_ids, 0),
            predictions=tf.equal(predictions, 0)
        ),
        'precision_1': tf.metrics.precision(
            labels=tf.equal(label_ids, 1),
            predictions=tf.equal(predictions, 1)
        ),
        'recall_1': tf.metrics.recall(
            labels=tf.equal(label_ids, 1),
            predictions=tf.equal(predictions, 1)
        ),
        'precision_2': tf.metrics.precision(
            labels=tf.equal(label_ids, 2),
            predictions=tf.equal(predictions, 2)
        ),
        'recall_2': tf.metrics.recall(
            labels=tf.equal(label_ids, 2),
            predictions=tf.equal(predictions, 2)
        )

    }


def tokens_to_chars(tokens, tokens_length, char_length):
    """
    Splits a list of tokens into unicode characters.
    This is an in-graph transformation.
    Args:
        tokens: A sequence of tokens.
    Returns:
        The characters as a ``tf.Tensor`` of shape
        ``[sequence_length, max_word_length]`` and the length of each word.
    """

    def _split_chars(token, max_length):
        padding_length = max_length - len(token)
        chars = list(token[:max_length]) + ['<PAD>']*padding_length
        return [chars]

    def _apply():

        chars = tf.map_fn(
            lambda x: tf.py_func(_split_chars, [x, char_length], [tf.string]),
            tokens,
            dtype=[tf.string],
            back_prop=False)

        return chars

    def _none():
        chars = tf.constant([], dtype=tf.string)
        return chars

    chars = tf.cond(tf.equal(tf.shape(tokens)[0], 0), true_fn=_none, false_fn=_apply)
    chars.set_shape([tokens_length, char_length])
    return chars


def lookup(x, path_to_vocabulary, num_oov_buckets=0):
    lookup_table = tf.contrib.lookup.index_table_from_file(
        vocabulary_file=path_to_vocabulary,
        num_oov_buckets=num_oov_buckets,
        vocab_size=None,
        default_value=1
    )
    return lookup_table.lookup(x)


def rev_lookup(x, path_to_vocabulary):
    lookup_table = tf.contrib.lookup.index_to_string_table_from_file(
        vocabulary_file=path_to_vocabulary
    )
    return lookup_table.lookup(x)


def lookup_embedder(x, path_to_vocab, embeddings):
    token_ids = lookup(x, path_to_vocab, num_oov_buckets=1)
    vocab_size = count_lines(path_to_vocab) + 1
    embedding_dim = embeddings.shape[1]
    return tf.contrib.layers.embed_sequence(
        token_ids,
        vocab_size=vocab_size,
        embed_dim=embedding_dim,
        initializer=tf.constant_initializer(embeddings),
        trainable=True
    )


def char_conv_embedder(x, path_to_vocab, embeddings, kernel_size=3, output_dim=128, char_length=20):
    chars = tf.map_fn(lambda t: tokens_to_chars(t, x.shape[1], char_length), x)
    embedded_chars = lookup_embedder(chars, path_to_vocab, embeddings)
    outputs = tf.layers.conv2d(
        embedded_chars,
        filters=output_dim,
        kernel_size=[1, kernel_size],
        strides=[1, 1],
        padding='VALID')

    # Max pooling over depth.
    outputs = tf.reduce_max(outputs, axis=2)

    return outputs


def elmo_embedder(tokens, lengths=[30]*64):
    elmo = hub.Module("https://tfhub.dev/google/elmo/2", trainable=True)
    # x = tf.squeeze(tf.cast(x, tf.string))
    return elmo(
        inputs={
            'tokens': tokens,
            'sequence_len': tf.clip_by_value(lengths, 0, 30)
        },
        signature="tokens",
        as_dict=True)['elmo']


def multi_label_classifier(x, classes_count):
    logits = tf.layers.dense(x, classes_count, activation=None)
    probs = tf.nn.softmax(logits)
    predictions = tf.argmax(probs, axis=1)
    return logits, probs, predictions


def binary_classifier(x):
    logits = tf.layers.dense(x, 1, activation=None)
    logits = tf.squeeze(logits)
    probs = tf.nn.sigmoid(logits)
    predictions = tf.cast(probs > 0.5, tf.int32)
    return logits, probs, predictions


def linear_classifier(x, params):
    return multi_label_classifier(x, params.classes_count) if params.multi_label else binary_classifier(x)


def weights_(label_ids, weights=[2.0, 1.0, 0.5]):
    ohe_labels = tf.one_hot(label_ids, depth=len(weights))
    return tf.reduce_sum(ohe_labels * weights, axis=1)


def compose_estimator(mode, labels, logits, probs, predictions, params):
    # mode: PREDICT
    if mode == tf.estimator.ModeKeys.PREDICT:
        predicted_labels = rev_lookup(predictions, params.label_vocab)
        return tf.estimator.EstimatorSpec(mode, predictions={'class': predicted_labels, 'prob': probs},
                                          export_outputs={'class': export_output.PredictOutput(predicted_labels)})

    # mode: TRAIN
    label_ids = lookup(labels, params.label_vocab)
    weights = 1
    if 'weights' in params: weights = weights_(label_ids, params.weights)
    if params.multi_label:
        loss = tf.losses.sparse_softmax_cross_entropy(label_ids, logits, weights=weights)
    else:
        loss = tf.losses.sigmoid_cross_entropy(label_ids, logits, weights=weights)
    if mode == tf.estimator.ModeKeys.TRAIN:
        train_op = tf.train.AdamOptimizer(params.learning_rate) \
            .minimize(loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)

    # mode: EVAL
    eval_metrics_ops = metrics(label_ids, predictions)
    return tf.estimator.EstimatorSpec(mode, loss=loss, eval_metric_ops=eval_metrics_ops)


def baseline_model(features, labels, mode, params):
    embeddings = init_embeddings(params.token_vocab, pretrained_embeddings=params.get('pretrained_embeddings'))
    embedded_input = lookup_embedder(features['text'], params.token_vocab, embeddings)
    embedded_list = tf.unstack(embedded_input, axis=1)

    rnn_cell_fw = tf.nn.rnn_cell.GRUCell(num_units=params.rnn_num_units)
    rnn_cell_bw = tf.nn.rnn_cell.GRUCell(num_units=params.rnn_num_units)
    _, encoding_fw, encoding_bw = tf.nn.static_bidirectional_rnn(rnn_cell_fw, rnn_cell_bw, embedded_list, dtype=tf.float32)
    encoding = tf.concat([encoding_fw, encoding_bw], axis=-1)

    logits, probs, predictions = linear_classifier(encoding, params)

    return compose_estimator(mode, labels, logits, probs, predictions, params)


def convolutional_model(features, labels, mode, params):
    embeddings = init_embeddings(params.token_vocab,  pretrained_embeddings=params.get('pretrained_embeddings'))
    embedded_input = lookup_embedder(features['text'], params.token_vocab, embeddings)
    # embedded_input = elmo_embedder(features['text'], features['length'])
    conv_features = []

    for kernel_size in params.kernel_sizes:
        conv = tf.layers.conv1d(embedded_input, filters=64, kernel_size=kernel_size, strides=1, padding='valid')
        conv = tf.reduce_max(conv, axis=1)
        conv_features.append(conv)
    encoding = tf.concat(conv_features, axis=1)
    logits, probs, predictions = linear_classifier(encoding, params)
    return compose_estimator(mode, labels, logits, probs, predictions, params)


def convolutional_recurrent_model(features, labels, mode, params):
    embeddings = init_embeddings(params.token_vocab)
    embedded_input = lookup_embedder(features['text'], params.token_vocab, embeddings)

    conv_features = []
    for kernel_size in params.kernel_sizes:
        conv = tf.layers.conv1d(embedded_input, filters=64, kernel_size=kernel_size, strides=1, padding='same')
        conv = tf.layers.max_pooling1d(conv, pool_size=3, strides=1, padding='valid')
        conv_features.append(conv)

    cnn_encoding = tf.concat(conv_features, axis=-1)
    embedded_list = tf.unstack(cnn_encoding, axis=1)

    rnn_cell_fw = tf.nn.rnn_cell.GRUCell(num_units=params.rnn_num_units)
    rnn_cell_bw = tf.nn.rnn_cell.GRUCell(num_units=params.rnn_num_units)
    _, encoding_fw, encoding_bw = tf.nn.static_bidirectional_rnn(rnn_cell_fw, rnn_cell_bw, embedded_list,
                                                                 dtype=tf.float32)
    rnn_encoding = tf.concat([encoding_fw, encoding_bw], axis=-1)
    logits, probs, predictions = linear_classifier(rnn_encoding, params)
    return compose_estimator(mode, labels, logits, probs, predictions, params)


def char_embedder_convolutional_model(features, labels, mode, params):
    embeddings = init_embeddings(params.char_vocab)
    embedded_input = char_conv_embedder(features['text'], params.char_vocab, embeddings)

    conv_features = []
    for kernel_size in params.kernel_sizes:
        conv = tf.layers.conv1d(embedded_input, filters=64, kernel_size=kernel_size, strides=1, padding='valid')
        conv = tf.reduce_max(conv, axis=1)
        conv_features.append(conv)

    encoding = tf.concat(conv_features, axis=1)
    logits, probs, predictions = linear_classifier(encoding, params)
    return compose_estimator(mode, labels, logits, probs, predictions, params)
