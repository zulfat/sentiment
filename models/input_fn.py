from util import _preprocess
import tensorflow as tf
import numpy as np


def get_input_fn(path_to_text, path_to_labels=None,
                 path_to_lengths=None, max_len=200,
                 batch_size=256, train=True,
                 charlevel=False, wordlevel=True):

    def tf_preprocessw(text):
        return tf.py_func(lambda txt: _preprocess(txt, max_len), [text], tf.string)

    def tf_preprocessc(text):
        return tf.py_func(lambda txt: _preprocess(txt, max_len, wordlevel=False), [text], tf.string)

    def _set_shape(t):
        t.set_shape([max_len])
        return t

    def input_fn():
        if path_to_labels is not None:
            labels_ds = tf.data.TextLineDataset(path_to_labels)
        else:
            labels_ds = None

        features = {}
        texts_ds = tf.data.TextLineDataset(path_to_text)
        if wordlevel:
            wordlevel_texts_ds = texts_ds.map(tf_preprocessw)
            wordlevel_texts_ds = wordlevel_texts_ds.map(_set_shape)
            features['text'] = wordlevel_texts_ds

        if charlevel:
            charlevel_texts_ds = texts_ds.map(tf_preprocessc)
            charlevel_texts_ds = charlevel_texts_ds.map(_set_shape)
            features['char'] = charlevel_texts_ds

        if path_to_lengths is not None:
            lengths_ds = np.loadtxt(path_to_lengths, dtype=np.int32)
            lengths_ds = tf.data.Dataset.from_tensor_slices(lengths_ds)
            features['length'] = lengths_ds

        if labels_ds is not None:
            dataset = tf.data.Dataset.zip((features, labels_ds))
        else:
            dataset = tf.data.Dataset.zip(features)

        if train:
            dataset = dataset.repeat()
            dataset = dataset.shuffle(1024)

        dataset = dataset.batch(batch_size)
        iterator = dataset.make_one_shot_iterator()
        return iterator.get_next()

    return input_fn


if __name__ == '__main__':
    lookup_table = tf.contrib.lookup.index_table_from_file(
        vocabulary_file='../vocabs/char_vocab',
        num_oov_buckets=0,
        vocab_size=None,
        default_value=0
    )

    train_input_fn = get_input_fn(
        path_to_text='../data/toxic_comments/comments_test.txt',
        path_to_labels='../data/toxic_comments/labels_test.txt',
        train=False,
        charlevel=True
    )
    features, labels = train_input_fn()
    lookup_op = lookup_table.lookup(features['char'])
    sess = tf.Session()
    sess.run(tf.tables_initializer())
    for i in range(1):
        tokens, token_ids = sess.run([features, lookup_op])
        print tokens['char'][0][0]
        print token_ids[0][0]
