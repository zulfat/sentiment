import nltk.tokenize as tokenizers
from gensim.models import KeyedVectors
from tensorflow.contrib.training import HParams
import numpy as np
import argparse
import codecs
import yaml

t_functions = {
    'word': tokenizers.word_tokenize,
    'wordpunct': tokenizers.wordpunct_tokenize,
    'none': lambda t: t
}


def pad_sequence(sequence, max_len=60, padding_value='<PAD>'):
    padding_size = max(max_len - len(sequence), 0)
    return sequence[:max_len] + [padding_value]*padding_size


def _preprocess(text, max_len=None, padding_value='<PAD>', wordlevel=True):
    processed_text = text.decode('utf-8').lower()
    if wordlevel:
        processed_text = tokenizers.word_tokenize(processed_text)
    else:
        processed_text = list(processed_text)
    if max_len is not None:
        processed_text = pad_sequence(
            processed_text,
            max_len=max_len,
            padding_value=padding_value
        )
    processed_text = [token.encode('utf-8') for token in processed_text]
    return [processed_text]


def build_vocab(texts, path_to_save, tokenize_f=tokenizers.word_tokenize, vocab_size=200000):
    vocab = {}
    for text in texts:
        for token in tokenize_f(text.lower()):
             if token not in vocab: vocab[token] = 0
             vocab[token] += 1

    vocab = sorted(vocab, key=lambda t: vocab[t], reverse=True)[:vocab_size]

    with codecs.open(path_to_save, 'w', encoding='utf-8') as output_stream:
        output_stream.write(u'<UNKNOWN>\n')
        output_stream.write(u'<PAD>\n')
        output_stream.write(u'\n'.join(vocab))


def count_lines(fname):
    with codecs.open(fname) as input_stream:
        for i, _ in enumerate(input_stream, start=1):
            pass
        return i


def load_config(config_file):

    with codecs.open(config_file, encoding='utf-8') as input_stream:
        params = yaml.load(input_stream)

    hparams = HParams(**params)
    return hparams


def init_embeddings(path_to_vocab, embedding_dim=200, pretrained_embeddings=None, normalize=True, binary=False):
    with codecs.open(path_to_vocab, encoding='utf-8') as input_stream:
        word2idx = {token: i for i, token in enumerate(input_stream)}

    embeddings = {}
    if pretrained_embeddings is not None:
        embeddings = KeyedVectors.load_word2vec_format(
            pretrained_embeddings,
            binary=binary,
            encoding='utf-8'
        )
        embedding_dim = embeddings.syn0.shape[1]

    pretrained = np.random.randn(len(word2idx), embedding_dim)
    for token, token_idx in word2idx.items():
        if token in embeddings: pretrained[token_idx] = embeddings[token]

    if normalize: pretrained /= np.linalg.norm(pretrained, axis=0)

    return pretrained


if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--infile')
    arg_parser.add_argument('--outfile')
    arg_parser.add_argument('--tokenizer', default='word')
    arg_parser.add_argument('--vsize', type=int, default=200000)
    args = arg_parser.parse_args()

    tokenize_f = t_functions[args.tokenizer]
    with codecs.open(args.infile, encoding='utf-8') as input_stream:
        build_vocab(input_stream, args.outfile, tokenize_f=tokenize_f, vocab_size=args.vsize)
