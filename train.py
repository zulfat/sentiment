import tensorflow as tf
from util import load_config
from models.input_fn import get_input_fn
from models.model_fn import convolutional_model


flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('config_file', 'configs/default.yml', 'Model configuration file')


def run_experiment(argv=None):

    params = load_config(FLAGS.config_file)
    run_config = tf.estimator.RunConfig(
        save_checkpoints_steps=params.save_checkpoints_steps,
        model_dir=params.model_dir
    )
    train_input_fn = get_input_fn(
        path_to_text=params.train_texts,
        path_to_labels=params.train_labels,
        path_to_lengths=params.train_lengths,
        max_len=params.max_len,
        batch_size=64,
        charlevel=True
    )
    eval_input_fn = get_input_fn(
        path_to_text=params.test_texts,
        path_to_labels=params.test_labels,
        path_to_lengths=params.test_lengths,
        max_len=params.max_len,
        train=False,
        charlevel=True
    )
    estimator = tf.estimator.Estimator(model_fn=convolutional_model, params=params, config=run_config)
    train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn, max_steps=params.train_steps)
    eval_spec = tf.estimator.EvalSpec(input_fn=eval_input_fn, throttle_secs=100)
    tf.estimator.train_and_evaluate(estimator=estimator, train_spec=train_spec, eval_spec=eval_spec)


if __name__ == '__main__':
    tf.logging.set_verbosity('INFO')
    tf.app.run(main=run_experiment)
