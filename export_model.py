import tensorflow as tf
from models.model_fn import baseline_model
import argparse
import os


if __name__ == '__main__':
    tf.logging.set_verbosity('INFO')
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--export_path')
    arg_parser.add_argument('--version')
    args = arg_parser.parse_args()
    export_path = os.path.join(
        tf.compat.as_bytes(args.export_path),
        tf.compat.as_bytes(args.version)
    )

    params = tf.contrib.training.HParams(
        learning_rate=0.002,
        n_classes=2,
        train_steps=10000,
        min_eval_frequency=1000,
        vocab_size=205068,
        rnn_num_units=200,
        token_vocab='vocabs/token_vocab',
        label_vocab='vocabs/label_vocab',
        max_len=200
    )

    run_config = tf.estimator.RunConfig(
        save_checkpoints_steps=500,
        model_dir='/tmp/solid'
    )
    estimator = tf.estimator.Estimator(model_fn=baseline_model, params=params, config=run_config)

    text_input = tf.placeholder(shape=[None, params.max_len], dtype=tf.string)
    length_input = tf.placeholder(shape=[None], dtype=tf.int32)
    model_input = tf.estimator.export.build_raw_serving_input_receiver_fn({"text": text_input})

    estimator.export_savedmodel(export_path, model_input)
