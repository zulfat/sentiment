from models.input_fn import get_input_fn
from models.model_fn import convolutional_model
from argparse import ArgumentParser
from util import load_config
from tqdm import tqdm
import tensorflow as tf
import numpy as np


if __name__ == '__main__':
    arg_parser = ArgumentParser()
    arg_parser.add_argument('--config')
    arg_parser.add_argument('--path_to_text')
    arg_parser.add_argument('--path_to_length')
    arg_parser.add_argument('--save_to')
    args = arg_parser.parse_args()
    params = load_config(args.config)
    input_fn = get_input_fn(
        path_to_text=args.path_to_text,
        path_to_lengths=args.path_to_length,
        max_len=params.max_len,
        train=False
    )
    probs = []
    estimator = tf.estimator.Estimator(model_fn=convolutional_model, params=params, model_dir=params.model_dir)

    for pred in tqdm(estimator.predict(input_fn), total=12411325):
        probs.append(pred['prob'])

    probs = np.stack(probs)
    np.savetxt('probs', probs)
