import numpy as np
from flask import Flask
from flask import request
from flask import jsonify
import tensorflow as tf
from util import _preprocess
from grpc.beta import implementations
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2


app = Flask(__name__)
host = '127.0.0.1'
port = 9001


def get_prediction(text):
    preprocessed_text = _preprocess(text, max_len=200)
    preprocessed_text = np.array(preprocessed_text, dtype=np.str)

    channel = implementations.insecure_channel(host, port)
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    r = predict_pb2.PredictRequest()
    r.model_spec.name = 'foo'
    r.inputs['text'].CopyFrom(tf.make_tensor_proto(preprocessed_text, dtype=tf.string))
    result = stub.Predict(r, 10.0)
    prediction = result.outputs['output'].bool_val
    return prediction


@app.route('/predict', methods=['POST'])
def predict():
    if request.json.get('data'):
        text = request.json['data']
        resp = get_prediction(text)
        return jsonify({'prediction': tuple(resp)})
    else:
        return jsonify({'status': 'error'})


if __name__ == '__main__':
    app.run()
